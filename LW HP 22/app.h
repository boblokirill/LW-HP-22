#pragma once
#include "Human/Human.h"
#include "vector.h"

template <class U>

void Sorting(Vector<U>& vector) {
	vector.sorting();
}

template <class U>

void Append(Vector<U>& vector) {
	vector.push_back();
}

template <class U>

void Delete(Vector<U>& vector) {
	vector.pop_back();
}

template <class U>

void Edit(Vector<U>& vector) {
	vector.edit();
}
