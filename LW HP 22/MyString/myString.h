//������������ ���� ������ myString
#pragma once
#include <iostream>

class myString
{
public:
	// ������������ // =============================================================================================================
	myString();                         // �����������
	myString(const char*);				// � ���������� ������� ���������� ������
	myString(const myString&);			// �����������
	~myString();						// ����������
	// ������������� ��������� // ==================================================================================================
	myString& operator =(const char*);				// ������������ ������ ������� ���������� ������
	myString& operator =(const myString&);			// ������������ ����� ������
	myString operator  +(const myString&);			// ������������� ����� ���� � ������ (����� + �����)
	friend myString operator +(const char*, myString&);
	myString operator += (const myString&);
	bool operator > (const myString&);
	bool operator < (const myString&);
	bool operator ==(const myString&);				// �������� ��������� ���� �����
	bool operator !=(const myString&);				// �������� ����������� ���� �����
	char& operator [](int index);					// ������ � ������������� ������� � ������

	friend std::ostream& operator <<(std::ostream& out, const myString& str);		// ����� ������ ����� <<
	friend std::istream& operator >> (std::istream& in, myString& str);				// ����  ������ ����� >>
	// ������ // ===================================================================================================================
	void Print();				// ����� ������
	int getSize();				// �������� ������ �������
private:
	char* string; 
	// ������� ������ // ===========================================================================================================
	bool emptyString() const;										// �������� �� ������� ������
	void copy(char* str, const char* str2, int i, int length);		// ����������� �����
	bool equal(const char* str, const char* str2);					// �������� �� ���������
};

