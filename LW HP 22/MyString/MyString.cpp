#include "myString.h"
#include <limits>

// ������� ������ // ===========================================================================================================

// ����������� �����
void myString::copy(char* str, const char* str2, int i = 0, int length = 0) {
	if (length == 0)
		length = strlen(str2);
	for (i; i < length; i++) {
		str[i] = str2[i];
	}
}

// �������� �� ���������
bool myString::equal(const char* str, const char* str2) {
	bool equal = true;
	int length1 = strlen(str);
	int length2 = strlen(str2);
	if (length1 == length2) {
		for (int i = 0; i < length1; i++) {
			if (str[i] != str2[i]) {
				equal = false;
			}
		}
	}
	else equal = false;
	return equal;
}

bool myString::emptyString() const {
	return string == nullptr ? true : false;
}

//===============================================================================================================================
/*
*
*
*
*/
// ������������ // ==============================================================================================================

myString::myString() {								// ����������� �����������
	string = nullptr;
	//cout << "����������� �� ����� \t" << this << endl;
}

myString::myString(const char* str)	{				// � �����������
	int length = strlen(str);
	this->string = new char[length + 1];
	copy(this->string, str);
	this->string[length] = '\0';
	//cout << "����������� � ���-�� \t" << this << endl;
}

myString::myString(const myString& other) {			// ����������� �����������
	if (!other.emptyString()) {
		int length = strlen(other.string);
		this->string = new char[length + 1];
		copy(this->string, other.string);
		this->string[length] = '\0';
	}
	//cout << "����������� ���-�� \t" << this << endl;
}

myString::~myString() {								// ����������
	delete[] string;
	//cout << "���������� \t\t" << this << endl;;
}

//===============================================================================================================================
/*
*
*
* 
*/
// ������������� ��������� // ===================================================================================================

// ������������ ������ ������� ���������� ������
myString& myString::operator=(const char* str) {				
	int length = strlen(str);
	if (this->string != nullptr) {
		delete[] this->string;
	}
	this->string = new char[length + 1];
	copy(this->string, str);
	this->string[length] = '\0';
	return *this;
}

// ������������ ����� ����c�
myString& myString::operator=(const myString& other) {
	if (!other.emptyString()) {
		int length = strlen(other.string);
		if (this->string != nullptr) {
			delete[] this->string;
		}
		this->string = new char[length + 1];
		copy(this->string, other.string);
		this->string[length] = '\0';
		return *this;
	}
}

// ������������� ����� ���� � ������ (����� + �����)
myString myString::operator+(const myString& other) {			
	myString newStr;
	int thisLength = strlen(this->string);
	int otherLength = strlen(other.string);
	newStr.string = new char[thisLength + otherLength + 1];
	int i;
	for (i = 0; i < thisLength; i++) {
		newStr.string[i] = this->string[i];
	}
	for (int j = 0; j < otherLength; j++, i++) {
		newStr.string[i] = other.string[j];
	}
	newStr.string[thisLength + otherLength] = '\0';
	return newStr;
}

// ������������� ����� ���� � ������ (����� + ���������� ������)
/*myString myString::operator +(const char* str) {
	myString newStr;
	int thisLength = strlen(this->string);
	int otherLength = strlen(str);
	newStr.string = new char[thisLength + otherLength + 1];
	int i;
	for (i = 0; i < thisLength; i++) {
		newStr.string[i] = this->string[i];
	}
	for (int j = 0; j < otherLength; j++, i++) {
		newStr.string[i] = str[j];
	}
	newStr.string[thisLength + otherLength] = '\0';
	return newStr;
}*/

// ������������� ����� ���� � ������ (���������� ������ + �����)
myString operator+(const char* str, myString& other) {
	myString newStr;
	int thisLength = strlen(str);
	int otherLength = strlen(other.string);
	newStr.string = new char[thisLength + otherLength + 1];
	int i;
	for (i = 0; i < thisLength; i++) {
		newStr.string[i] = str[i];
	}
	for (int j = 0; j < otherLength; j++, i++) {
		newStr.string[i] = other.string[j];
	}
	newStr.string[thisLength + otherLength] = '\0';
	return newStr;
}

myString myString::operator+=(const myString& other) {
	int thisLength = strlen(this->string);
	int otherLength = strlen(other.string);
	char* temp = new char[thisLength];
	copy(temp, this->string);
	delete[] this->string;
	this->string = new char[thisLength + otherLength + 1];
	int i;
	for (i = 0; i < thisLength; i++) {
		this->string[i] = temp[i];
	}
	for (int j = 0; j < otherLength; j++, i++) {
		this->string[i] = other.string[j];
	}
	this->string[thisLength + otherLength] = '\0';
	return *this;
}

bool myString::operator>(const myString& other) {
	bool correct{};
	int length = strlen(this->string);
	int length2 = strlen(other.string);
	int minLength = length < length2 ? length : length2;
	int count = 0;
	bool flag = true;
	for (int i = 0; i < minLength; i++) {
		if ((this->string[i] > other.string[i]) && (flag == true)) {
			correct = true;
			flag   = false;
		}
		else if ((this->string[i] == other.string[i]) && (flag == true)) {
			++count;
		}
		else flag = false;
	}
	if (count == minLength) {
		correct = true;
	}
	// dfg dfc
	return correct;
}

bool myString::operator<(const myString& other) {
	bool correct{};
	int length = strlen(this->string);
	for (int i = 0; i < length; i++) {
		if (this->string[i] < other.string[i]) {
			correct = true;
		}
	}
	return correct;
}

// �������� ��������� ���� �����
bool myString::operator==(const myString& other) {				
	return equal(this->string, other.string);
}

// �������� ����������� ���� �����
bool myString::operator!=(const myString& other) {				
	return !(equal(this->string, other.string));
}

// ������ � ������������� ������� � ������
char& myString::operator[](int index) {
	return this->string[index];
}

// ����� ������ ����� <<
std::ostream& operator<<(std::ostream& out, const myString& str) {
	out << str.string;
	return out;
}

// ����  ������ ����� >>
std::istream& operator >> (std::istream& in, myString& str) {
	char BUFF[2048];
	in.getline(BUFF, sizeof BUFF);
	str = BUFF;
	return in;
}

//===============================================================================================================================
/*
*
*
*
*/
// ������ // ===================================================================================================================

// ����� ������
void myString::Print() {
	std::cout << this->string;
}

// �������� ������ �������
int myString::getSize() {
	return strlen(this->string);
}


