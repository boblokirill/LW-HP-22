#include "MyConMenu/Item.h"
#include "MyConMenu/Cmenu.h"
#include "Human/Human.h"
#include "Date.h"
#include "Vector.h"
#include "app.h"
#include "MyCinema/Hall.h"
#include "MyCinema/Film.h"
#include "Human/Administrator.h"
#include "Human/User.h"
#include "MyCinema/Session.h"
#include <vector>
using namespace BKM;

int Human::s_idGenerator = 0;
int Human::s_countPeople = 0;

Vector<Administrator> adminVector;
Vector<User>		  UserVector;
Vector<Film>		  filmVector;
Vector<Hall>		  hallVector;
Vector<Session>		  sessionVector;

int AddAdmin()	 { Append(adminVector);   return 0; }
int AddUser()	 { Append(UserVector);    return 0; }
int AddFilm()	 { Append(filmVector);	  return 0; }
int AddHall()    { Append(hallVector);	  return 0; }
int AddSession() { Append(sessionVector); return 0; }

int DeleteAdmin()	{ Delete(adminVector);	 return 0; }
int DeleteUser()	{ Delete(UserVector);	 return 0; }
int DeleteFilm()	{ Delete(filmVector);    return 0; }
int DeleteHall()    { Delete(hallVector);    return 0; }
int DeleteSession() { Delete(sessionVector); return 0; }

int EditAdmin()   { Edit(adminVector);   return 0; }
int EditUser()	  { Edit(UserVector);    return 0; }
int EditFilm()    { Edit(filmVector);    return 0; }
int EditHall()    { Edit(hallVector);	 return 0; }
int EditSession() { Edit(sessionVector); return 0; }

int SortingAdmin()   { Sorting(adminVector);   return 0; }
int SortingUser()    { Sorting(UserVector);    return 0; }
int SortingFilm()    { Sorting(filmVector);    return 0; }
int SortingHall()    { Sorting(hallVector);	   return 0; }
int SortingSession() { Sorting(sessionVector); return 0; }

int PrintAdmin()   { adminVector.print();   return 0; }
int PrintUser()    { UserVector.print();    return 0; }
int PrintFilm()    { filmVector.print();    return 0; }
int PrintHall()    { hallVector.print();	return 0; }
int PrintSession() { sessionVector.print(); return 0; }


int AddObject() {
	const size_t countItemMenu = 5;
	Item itemObject[countItemMenu]{
		{"1. �������������", AddAdmin  },
		{"2. ������������",  AddUser   },
		{"3. �����",		 AddFilm   },
		{"4. ���",			 AddHall   },
		{"5. �����",		 AddSession}
	};
	Cmenu MenuAddObject{ "��������: ", itemObject, countItemMenu };
	cout << MenuAddObject;
	cin  >> MenuAddObject;
	return 0;
}

int DeleteObject() {
	const size_t countItemMenu = 5;
	Item itemObject[countItemMenu]{
		{"1. �������������", DeleteAdmin  },
		{"2. ������������",  DeleteUser   },
		{"3. �����",		 DeleteFilm   },
		{"4. ���",			 DeleteHall   },
		{"5. �����",		 DeleteSession}
	};
	Cmenu MenuAddObject{ "�������: ", itemObject, countItemMenu };
	cout << MenuAddObject;
	cin >> MenuAddObject;
	return 0;
}

int EditObject() {
	const size_t countItemMenu = 5;
	Item itemObject[countItemMenu]{
		{"1. �������������", EditAdmin  },
		{"2. ������������",  EditUser   },
		{"3. �����",		 EditFilm   },
		{"4. ���",			 EditHall   },
		{"5. �����",		 EditSession}
	};
	Cmenu MenuAddObject{ "��������: ", itemObject, countItemMenu };
	//cout << MenuAddObject;
	cin >> MenuAddObject;
	return 0;
}

int SortingObject() {
	const size_t countItemMenu = 5;
	Item itemObject[countItemMenu] {
		{"1. �������������", SortingAdmin  },
		{"2. ������������",	 SortingUser   },
		{"3. �����",		 SortingFilm   },
		{"4. ���",			 SortingHall   },
		{"5. �����",		 SortingSession}
	};
	Cmenu MenuAddObject{ "�������������: ", itemObject, countItemMenu };
	cout << MenuAddObject;
	cin >> MenuAddObject;
	return 0;
}

int Print() {
	const size_t countItemMenu = 5;
	Item itemObject[countItemMenu]{
		{"1. �������������", PrintAdmin  },
		{"2. ������������",  PrintUser   },
		{"3. �����",		 PrintFilm   },
		{"4. ���",			 PrintHall   },
		{"5. �����",		 PrintSession}
	};
	Cmenu MenuAddObject{ "�����������: ", itemObject, countItemMenu };
	cout << MenuAddObject;
	cin >> MenuAddObject;
	return 0;
}

int main() {
	setlocale(LC_ALL, "ru");
	SetConsoleTitle(L"������������������ ������� ����������");
	const size_t countItem = 5;
	Item item[countItem]{
		{"1. ��������.",	AddObject    },
		{"2. �������.",		DeleteObject },
		{"3. ��������.",	EditObject   },
		{"4. �����������.",	SortingObject},
		{"5. �����������.",	Print        }
	};


	Cmenu Menu("\n������������������ ������� ����������", item, countItem);

	while (true) {
		cout << Menu;
		cin  >> Menu;
	}
	return 0;	
}