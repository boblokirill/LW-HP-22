#pragma once
#include "Item.h"
using namespace BKM;

AItemMenu::AItemMenu(const char* ItemName, Funk pFunk) {
	m_item_name = new char[256];
	strcpy_s(m_item_name, 256, ItemName);
	m_funk = pFunk;
}

AItemMenu::~AItemMenu() {
	delete[] m_item_name;
}

char* AItemMenu::getItemName() {
	return m_item_name;
}

int AItemMenu::run() {
	return m_funk();
}
/*
char BKM::AItemMenu::getChar(int index)
{
	return m_item_name[index];
}

char Item::getChar(int index) {
	return m_item_name[index];
}*/