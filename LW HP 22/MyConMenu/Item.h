#pragma once
#include "AbstractItemMenu.h"
#include "Color.h";
#include <Windows.h>
namespace BKM {
	class Item : public AItemMenu  {
	public:
		Item(const char*, Funk);
		~Item();
		void printItemName() override;
		void printItemName(int color) override;
		//char getChar(int index) override;
	};
}