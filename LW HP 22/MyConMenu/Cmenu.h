#pragma once
#include "Item.h"
using namespace BKM;

class Cmenu {
public:
	Cmenu(const char* title, AItemMenu* items, size_t count);
	~Cmenu();
	void Print(int _index = 0);
	int runCommand();

	friend std::ostream& operator<<(std::ostream& out,Cmenu& menu);
	friend std::istream& operator>>(std::istream& in, Cmenu& menu);
private:
	int m_select = -1;
	bool m_running = false;
	char* m_title = nullptr;
	size_t m_count;
	AItemMenu* m_items = nullptr;
	HANDLE* m_console = nullptr;
};
