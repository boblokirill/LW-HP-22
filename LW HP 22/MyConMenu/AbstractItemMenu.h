#pragma once
#include <iostream>
#include "Color.h"
namespace BKM {
	class AItemMenu {
	public:
		typedef int(*Funk)();
		AItemMenu(const char*, Funk);
		AItemMenu(const char*);
		~AItemMenu();	
		virtual char* getItemName();
		virtual void printItemName() = 0;
		virtual void printItemName(int color) = 0;
		virtual int run();
		//virtual char getChar(int index) = 0;
	protected:
		char* m_item_name = nullptr;
		Funk m_funk = nullptr;
	};
}