#include "Cmenu.h"
#include <conio.h>
#include "Color.h"
#include "Command.h"
using namespace BKM;

Cmenu::Cmenu(const char* title, AItemMenu* items, size_t count) {
	m_title = new char[256];
	strcpy_s(m_title, 256, title);
	m_items = items;
	m_count = count;
}

Cmenu::~Cmenu() {
	delete[] m_title;
}

void Cmenu::Print(int _index) {
	std::cout << this->m_title << std::endl;
	for (int i = 0; i < m_count; i++) {
		m_items[i].printItemName();
	}
}

int Cmenu::runCommand() {
	m_select = -1;
	std::cout << "-> ";
	std::cin >> m_select;
	std::cout << "\n";
	m_select--;
	return m_items[m_select].run();
}

std::ostream& operator<<(std::ostream& out, Cmenu& menu) {
	menu.Print();
	return out;
}

std::istream& operator>>(std::istream& in, Cmenu& menu) {
	menu.runCommand();
	return in;
}
