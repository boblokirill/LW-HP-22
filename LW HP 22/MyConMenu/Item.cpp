#pragma once
#include "Item.h"
using namespace BKM;

Item::Item(const char* ItemName, Funk pFunk) : AItemMenu(ItemName, pFunk) {

}


Item::~Item() {

}

void Item::printItemName() {
	std::cout << Item::getItemName() << std::endl;
}

void Item::printItemName(int color) {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
	std::cout << Item::getItemName() << std::endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), LIGHT_GRAY);
}

/*char Item::getChar(int index) {
	//return m_item_name[index];
	return 'x';
}*/