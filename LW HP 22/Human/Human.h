#pragma once
#include "/�����/21�/��/LW HP 22/LW HP 22/MyString/myString.h"
#include "/�����/21�/��/LW HP 22/LW HP 22/Date.h"
#include <iostream>
using namespace std;

class Human {
public:
	static int s_idGenerator;
	static int s_countPeople;

	Human();
	Human(const char* firstName, const char* lastName, const char* login, const char* m_password);
	Human(const char* login, const char* m_password);
	Human(const Human&);
	~Human();

	void Print() const;
	void set();
	//void setName(char* firstName, char* lastName);
	//int setLogin();

	Human& operator=(const Human&);
	bool   operator>(const Human&);

	friend ostream& operator<<(ostream& out, Human& human);
	friend istream& operator>>(istream& in, Human& human);
private:
	myString m_firstName;
	myString m_lastName;
	myString m_login;
	myString m_password;
	unsigned long int m_id;
	Date m_date;
};