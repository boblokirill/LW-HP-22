#include "Human.h"
#include <iostream>
//using namespace BKM;


//������������ � ����������� ======================================================================
Human::Human() {
	this->m_firstName = "-";
	this->m_lastName  = "-";
	this->m_login     = "-";
	this->m_password  = "-";
	this->m_id = s_idGenerator;

	s_idGenerator++;
	++s_countPeople;
}

Human::Human(const char* firstName, const char* lastName, const char* login, const char* password) {
	this->m_login     =	login;
	this->m_password  =	password;
	this->m_firstName = firstName;
	this->m_lastName  =	lastName;
	this->m_id        =	s_idGenerator;

	s_idGenerator++;
	++s_countPeople;
}

Human::Human(const char* login, const char* password) {
	this->m_login		= login;
	this->m_password	= password;
	this->m_firstName	= "�� ��������";
	this->m_lastName	= "�� ��������";
	this->m_id			= s_idGenerator;

	s_idGenerator++;
	++s_countPeople;
}

Human::Human(const Human& other) {
	this->m_firstName = other.m_firstName;
	this->m_lastName  = other.m_lastName;
	this->m_login     = other.m_login;
	this->m_password  = other.m_password;
	this->m_id		  = other.m_id;
	this->m_date	  = other.m_date;
}

Human::~Human() {
	--s_countPeople;
}

// ������ =========================================================================================

void Human::Print() const {
	cout << "���:    \t" << this->m_firstName << endl;
	cout << "�������:\t" << this->m_lastName << endl;
	cout << "�����:  \t" << this->m_login << endl;
	cout << "������: \t" << this->m_password << endl;
	cout << "Id:	 \t" << this->m_id << endl;
	cout << "���� ��������:\t" << this->m_date << endl;
}

void Human::set() {
	cin.ignore();
	cout << "������� ���:     \t"; cin >> m_firstName;
	cout << "������� �������: \t"; cin >> m_lastName;
	cout << "������� �����:   \t"; cin >> m_login;
	cout << "������� ������:  \t"; cin >> m_password;
}

// ���������� ���������� ==========================================================================

Human& Human::operator=(const Human& human) {
	this->m_firstName = human.m_firstName;
	this->m_lastName  =	human.m_lastName;
	this->m_login     =	human.m_login;
	this->m_password  =	human.m_password;
	this->m_id        =	human.m_id;
	this->m_date      =	human.m_date;

	return *this;
}

bool Human::operator>(const Human& human) {
	bool correct{};
	if (this->m_lastName > human.m_lastName) {
		correct = true;
	}
	else if (this->m_lastName == human.m_lastName) {
		if (this->m_firstName > human.m_firstName) {
			correct = true;
		}
		else if (this->m_firstName == human.m_firstName) {
			if (this->m_id > human.m_id) {
				correct = true;
			}
		}
		else correct = false;
	}
	else correct = false;
	return correct;
}

ostream& operator<<(ostream& out, Human& human) {
	out << "���:    \t" << human.m_firstName << "\n"
		<< "�������:\t" << human.m_lastName << "\n"
		<< "�����:  \t" << human.m_login << "\n"
		<< "������: \t" << human.m_password << "\n"
		<< "Id:	    \t" << human.m_id << "\n\n";
	return out;
}

istream& operator>>(istream& in, Human& human) {
	human.set();
	return in;
}