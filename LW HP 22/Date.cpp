#include "Date.h"
#include <iostream>

Date::Date() {
	m_seconds = time(NULL);
	timeTransform();
}

Date::Date(time_t seconds) {
	m_seconds = seconds;
	timeTransform();
}

Date::~Date() {

}

void Date::timeTransform() {
	m_g = m_seconds / 31536000;
	m_v = m_g ? (m_g % 4 == 0 && m_g % 100 != 0 || m_g % 400 == 0) : false;
	m_Year = m_seconds / (m_v ? 31622400 : 31536000) + 1970;
	m_Month = (m_seconds / (m_v ? 2635200 : 2628000)) % 12;
	m_Day = (m_seconds / 86400) % (m_Month ? (28 + ((0x3bbeecc >> (2 * m_Month)) & 3)) : 31) + 7;
	m_hour = (m_seconds / 3600) % 24 + 3;
	m_Month = (m_seconds / 60) % 60;
	m_sec = m_seconds % 60;
}

time_t Date::getSeconds() {
	return m_seconds;
}

void Date::Print() {
	printf_s("%02d/%02d/%04d %02d:%02d:%02d\n\n", this->m_Day, this->m_Month, this->m_Year, this->m_hour, this->m_Month, this->m_sec);
}

void Date::set() {
	std::cout << "������� ���:    \t"; std::cin >> m_Year;
	std::cout << "������� �����:  \t"; std::cin >> m_Month;
	std::cout << "������� ����:   \t"; std::cin >> m_Day;
	std::cout << "������� ���:    \t"; std::cin >> m_hour;
	std::cout << "������� ������: \t"; std::cin >> m_Month;
}

std::ostream& operator<<(std::ostream& out, const Date& date)
{
	out << date.m_Day << "/" << date.m_Month << "/" << date.m_Year << " " << date.m_hour << ":" << date.m_Month;
	return out;
}

std::istream& operator>>(std::istream& in, Date& date) {
	date.set();
	return in;
}
