#pragma once
#include <iostream>
template <class T>

class Vector {
public:
	Vector() {
		m_data = nullptr;
		m_count = 0;
	}
	~Vector() {
		delete[] m_data;
		m_data = nullptr;
	}
	Vector(T other) {
		m_data = other;
	}

	size_t getCount() {
		return m_count;
	}

	void print() {
		if (!(m_data == nullptr)) {
			for (int i = 0; i < m_count; i++) {
				std::cout << m_data[i] << endl;
			}
		}
		else cout << "������	 ����. " << endl;
	}

	void push_back() {
		T NewData{};
		cin >> NewData;
		T* newVector = new T[m_count + 1];
		for (int i = 0; i < m_count; i++) {
			newVector[i] = m_data[i];
		}
		newVector[m_count] = NewData;
		delete[] m_data;
		m_data = newVector;
		m_count++;
	}

	void pop_back() {
		T* newVector = new T[m_count - 1];
		for (int i = 0; i < m_count - 1; i++) {
			newVector[i] = m_data[i];
		}
		delete[] m_data;
		m_data = newVector;
		m_count--;
	}

	void edit() {
		int index{};
		if (!(m_data == nullptr)) {
			for (int i = 0; i < m_count; i++) {
				cout << "____________________________" << endl;
				cout << "� " << i + 1 << endl;
				cout << m_data[i];
				cout << "____________________________" << endl;
			}
			cout << "\n������� ����� ������� �������� ������ ��������: ";
			cin >> index;
			index--;
			cin >> m_data[index];
		}
		else cout << "������ ����\n";
	}

	void sorting() {
		for (int i = 0; i < m_count - 1; i++) {
			for (int j = 0; j < m_count - i - 1; j++) {
				if (m_data[j] > m_data[j + 1]) {
					swap(m_data[j], m_data[j + 1]);
				}
			}
		}
	}

protected:
	T* m_data;
	size_t m_count;
};

