#pragma once
#include <iostream>

	class Hall
	{
	public:
		Hall();
		~Hall();

		void Print();

		bool operator > (Hall&);

		friend std::ostream& operator << (std::ostream& out, Hall&);
		friend std::istream& operator >> (std::istream& in, Hall&);
	private:
		int m_numberHall;
	};

