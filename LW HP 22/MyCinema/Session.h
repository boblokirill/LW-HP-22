#pragma once
#include "Date.h"
#include "Film.h"
#include "Hall.h"
class Session
{
public:
	Session();
	~Session();

	void NewDate();

	bool operator>(Session&);

	friend std::ostream& operator << (std::ostream& out, Session&);
	friend std::istream& operator >> (std::istream& in, Session&);
private:
	Hall m_Hall;
	Film m_Film;
	Date m_DayateSession;
};

