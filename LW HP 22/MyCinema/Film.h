#pragma once
#include "MyString/myString.h"
class Film
{
public:
	Film();
	~Film();

	myString getNameFilm();

	bool operator > (Film&);

	friend std::ostream& operator << (std::ostream& out, Film&);
	friend std::istream& operator >> (std::istream& in, Film&);
private:
	myString m_NameFilm;
};

