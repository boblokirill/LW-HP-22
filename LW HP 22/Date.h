#pragma once
#include <ctime>
#include <iostream>
class Date {
public:
	Date();
	Date(time_t);
	~Date();

	void   Print();
	time_t getSeconds();
	void   set();

	friend std::ostream& operator<< (std::ostream& out, const Date& date);
	friend std::istream& operator>> (std::istream& in, Date& date);
private:
	time_t m_seconds;
	long int m_g, m_Year, m_Month, m_Day, m_hour, m_minute, m_sec;
	bool m_v;
	void timeTransform();
};

